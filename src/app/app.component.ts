import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PostsService } from './services/posts.service';
import { PostsWithPhoto } from './interfaces/posts.interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  posts: PostsWithPhoto[] = [];

  constructor(private postsService: PostsService) { }

  ngOnInit(): void {
    this.loadPosts();
  }

  loadPosts(): void {
    forkJoin({
      posts: this.postsService.getPosts(),
      photos: this.postsService.getPhotos()
    }).pipe(
      map((forkJoinResponse) => {
        return forkJoinResponse.posts.map((post) => {
          delete post.userId;
          const photo = forkJoinResponse.photos.find(currentPhoto => currentPhoto.id === post.id);
          return Object.assign(post, { imageSrc: photo.url }) as PostsWithPhoto;
        });
      })
    ).subscribe({
      next: postsWithPhotos => this.posts = postsWithPhotos
    });
  }

}
