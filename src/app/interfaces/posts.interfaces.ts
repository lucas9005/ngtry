export interface Posts {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export interface Photos {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

export interface PostsWithPhoto {
  imageSrc: string;
  id: number;
  title: string;
  body: string;
}
