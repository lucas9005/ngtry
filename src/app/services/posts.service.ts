import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Photos, Posts } from '../interfaces/posts.interfaces';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  postsUrl = 'https://jsonplaceholder.typicode.com/posts';
  photosUrl = 'https://jsonplaceholder.typicode.com/photos';

  constructor(private http: HttpClient) { }

  public getPosts(): Observable<Posts[]> {
    return this.http.get<Posts[]>(this.postsUrl);
  }

  public getPhotos(): Observable<Photos[]> {
    return this.http.get<Photos[]>(this.photosUrl);
  }

}
