import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-simple-pane',
  templateUrl: './simple-pane.component.html',
  styleUrls: ['./simple-pane.component.scss']
})
export class SimplePaneComponent {

  @Input() imageSrc: string;
  @Input() id: number;
  @Input() title: string;
  @Input() body: string;

}
