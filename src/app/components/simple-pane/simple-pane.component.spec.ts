import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimplePaneComponent } from './simple-pane.component';

describe('SimplePaneComponent', () => {
  let component: SimplePaneComponent;
  let fixture: ComponentFixture<SimplePaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimplePaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimplePaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
